<?php

namespace Mageiran\CustomMenu\Plugin;

class Topmenu
{
    /**
    * @param Context                                   $context
    * @param array                                     $data
    */
    public function __construct(
        \Magento\Customer\Model\Session $session
    ) {
        $this->Session = $session;
    }


    public function afterGetHtml(\Magento\Theme\Block\Html\Topmenu $topmenu, $html)
    {
        $swappartyUrl = $topmenu->getUrl('/');//here you can set link
        $magentoCurrentUrl = $topmenu->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        if (strpos($magentoCurrentUrl,'/') !== false) {
            $custom_menu = "<li class=\"level0 nav-5 active level-top parent ui-menu-item\">";
        } else {
            $custom_menu = "<li class=\"level0 nav-4 level-top parent ui-menu-item\">";
        }
        $custom_menu .= "<a href=\"" . $swappartyUrl . "\" class=\"level-top ui-corner-all\"><span class=\"ui-menu-icon ui-icon ui-icon-carat-1-e\"></span><span>" . __("Home") . "</span></a>";
        $custom_menu .= "</li>";
        $custom_menu .= $html;
        return $custom_menu;
    }
}